package com.example.tick.eazydocker_demo;

import android.app.ListActivity;

/**
 * Created by tick on 9/24/2016.
 */
import android.app.ProgressDialog;
import android.os.AsyncTask;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import android.os.Bundle;
public class viewprocess extends ListActivity {

    private String TAG = viewprocess.class.getSimpleName();
    private ProgressDialog pDialog;
    //private ListView lv;
    // URL to get contacts JSON
    private static String url = "http://128.199.173.55/api_getprocess.py?container=narin";
    //private static String url = "http://128.199.173.55/api_unpause.py?container=Domy";
    ArrayList<HashMap<String, String>> contactList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.all_images);

        contactList = new ArrayList<>();

        //lv = (ListView) findViewById(R.id.);
        new GetContacts().execute();
        ListView lv = getListView();
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // getting values from selected ListItem
                String pid = ((TextView) view.findViewById(R.id.pid)).getText()
                        .toString();


            }
        });
    }

    private class GetContacts extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(viewprocess.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();

            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url);

            Log.e(TAG, "Response from url: " + jsonStr);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    //JSONArray jObj = new JSONArray(jsonStr);

                    // Getting JSON Array node
                    JSONArray contacts = jsonObj.getJSONArray("Processes");
                    // looping through All Contacts
                    for (int i = 0; i < contacts.length(); i++) {
                        JSONArray c = contacts.getJSONArray(i);

                        String user = c.getString(0);
                        String pid = c.getString(1);
                        String cpu = c.getString(2);
                        String mem = c.getString(3);
                        String start = c.getString(8);

                        //String address = c.getString("address");
                        //String gender = c.getString("gender");

                        // Phone node is JSON Object
                        //JSONObject phone = c.getJSONObject("phone");
                        //String mobile = phone.getString("mobile");
                        //String home = phone.getString("home");
                        //String office = phone.getString("office");

                        // tmp hash map for single contact
                        HashMap<String, String> contact = new HashMap<>();

                        // adding each child node to HashMap key => value
                        contact.put("user", user);
                        contact.put("pid", pid);
                        contact.put("cpu", cpu);
                        contact.put("mem", mem);
                        contact.put("start", start);
                        //contact.put("mobile", mobile);

                        // adding contact to contact list
                        contactList.add(contact);
                    }
                } catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });

                }
            } else {
                Log.e(TAG, "Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */
            ListAdapter adapter = new SimpleAdapter(
                    viewprocess.this, contactList,
                    R.layout.all_process, new String[]{"user", "pid",
                    "cpu", "mem", "start"}, new int[]{R.id.user,
                    R.id.pid, R.id.cpu, R.id.mem, R.id.start});

            setListAdapter(adapter);
        }

    }


}
