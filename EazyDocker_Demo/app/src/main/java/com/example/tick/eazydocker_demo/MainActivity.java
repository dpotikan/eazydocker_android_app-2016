package com.example.tick.eazydocker_demo;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.content.Intent;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    Button view_list_images;
    Button view_list_containers;
    Button getView_list_containers_all;
    Button view_process;
    public String url = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        view_list_images = (Button)findViewById(R.id.b1);
        view_list_images.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                // Launching All products Activity
                Intent i = new Intent(getApplicationContext(), listImages.class);
                startActivity(i);
            }
        });

        view_list_containers = (Button) findViewById(R.id.b2);
        view_list_containers.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                // Launching All products Activity
                Intent i = new Intent(getApplicationContext(), listContainers.class);
                startActivity(i);
            }
        });

        getView_list_containers_all = (Button) findViewById(R.id.b3);
        getView_list_containers_all.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                // Launching All products Activity
                Intent i = new Intent(getApplicationContext(), listContainersall.class);
                startActivity(i);
            }
        });

        view_process = (Button) findViewById(R.id.b4);
        view_process.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                // Launching All products Activity
                Intent i = new Intent(getApplicationContext(), viewprocess.class);
                startActivity(i);
            }
        });

        view_process = (Button) findViewById(R.id.restart);
        view_process.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                // Launching All products Activity
                url = "http://128.199.173.55/api_restart.py?container=Domy";
                new GetContacts().execute();



            }
        });

        view_process = (Button) findViewById(R.id.pause);
        view_process.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                // Launching All products Activity
                url = "http://128.199.173.55/api_pause.py?container=Domy";
                new GetContacts().execute();
            }
        });

        view_process = (Button) findViewById(R.id.unpause);
        view_process.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                // Launching All products Activity
                url = "http://128.199.173.55/api_unpause.py?container=Domy";
                new GetContacts().execute();
            }
        });

        view_process = (Button) findViewById(R.id.rename);
        view_process.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                // Launching All products Activity
                url = "http://128.199.173.55/api_rename.py?container=None&name=k2";
                new GetContacts().execute();
            }
        });

        view_process = (Button) findViewById(R.id.stop);
        view_process.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                // Launching All products Activity
                Intent i = new Intent(getApplicationContext(), Stop.class);
                startActivity(i);
            }
        });


    }
    class GetContacts extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();

            // Making a request to url and getting response
            sh.makeServiceCall(url);


            return null;

        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog

        }

    }
}
