package com.example.fannies.projectdocker.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.fannies.projectdocker.R;

/**
 * Created by Fannies on 1/19/2017.
 */

public class ListHostHolder extends RecyclerView.ViewHolder {

    private ImageView ivType;
    private TextView tvHostName;
    private TextView tvHostIp;

    public ImageView getIvType() {
        return ivType;
    }

    public void setIvType(ImageView ivType) {
        this.ivType = ivType;
    }

    public TextView getTvHostName() {
        return tvHostName;
    }

    public void setTvHostName(TextView tvHostName) {
        this.tvHostName = tvHostName;
    }

    public TextView getTvHostIp() {
        return tvHostIp;
    }

    public void setTvHostIp(TextView tvHostIp) {
        this.tvHostIp = tvHostIp;
    }

    public ListHostHolder(View itemView) {
        super(itemView);

        ivType = (ImageView) itemView.findViewById(R.id.ivType);
        tvHostName = (TextView) itemView.findViewById(R.id.tvHostName);
        tvHostIp = (TextView) itemView.findViewById(R.id.tvHostIp);
    }
}
