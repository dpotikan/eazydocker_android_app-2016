package com.example.fannies.projectdocker.dao;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Fannies on 1/24/2017.
 */

public class DockerContainerEachStatCollectionDao {
    @SerializedName("Data")     private List<DockerContainerEachStatDao> data;
    @SerializedName("status")   private Boolean status;

    public List<DockerContainerEachStatDao> getData() {
        return data;
    }

    public void setData(List<DockerContainerEachStatDao> data) {
        this.data = data;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
