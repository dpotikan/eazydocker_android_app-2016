package com.example.fannies.projectdocker.dao;

import com.google.gson.annotations.SerializedName;

/**
 * Created by NARIN on 26/1/2560.
 */

public class DockerContainerPeakDao
{
    @SerializedName("Cpu_per")  private Float cpuPer;
    @SerializedName("Date")     private String date;
    @SerializedName("C_name")   private String cName;
    @SerializedName("M_per")    private Float mPer;

    public Float getCpuPer() {
        return cpuPer;
    }

    public void setCpuPer(Float cpuPer) {
        this.cpuPer = cpuPer;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getcName() {
        return cName;
    }

    public void setcName(String cName) {
        this.cName = cName;
    }

    public Float getmPer() {
        return mPer;
    }

    public void setmPer(Float mPer) {
        this.mPer = mPer;
    }
}
