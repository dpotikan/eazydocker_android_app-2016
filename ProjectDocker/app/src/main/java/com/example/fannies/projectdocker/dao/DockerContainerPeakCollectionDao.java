package com.example.fannies.projectdocker.dao;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by NARIN on 26/1/2560.
 */

public class DockerContainerPeakCollectionDao {
    @SerializedName("Data")     private List<DockerContainerPeakDao> data;
    @SerializedName("Status")   private  boolean status;

    public List<DockerContainerPeakDao> getData() {
        return data;
    }

    public void setData(List<DockerContainerPeakDao> data) {
        this.data = data;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
