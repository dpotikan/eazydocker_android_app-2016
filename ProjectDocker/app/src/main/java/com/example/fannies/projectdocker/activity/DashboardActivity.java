package com.example.fannies.projectdocker.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fannies.projectdocker.R;
import com.example.fannies.projectdocker.adapter.HostListAdapter;
import com.example.fannies.projectdocker.dao.DockerHostDao;
import com.example.fannies.projectdocker.fragment.AboutFragment;
import com.example.fannies.projectdocker.fragment.DashboardFragment;
import com.example.fannies.projectdocker.manager.DockerContainerManager;
import com.example.fannies.projectdocker.manager.HostListManager;
import com.example.fannies.projectdocker.manager.HttpManager;
import com.example.fannies.projectdocker.manager.UserManager;
import com.inthecheesefactory.thecheeselibrary.manager.Contextor;

import org.w3c.dom.Text;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DashboardActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private Toolbar toolbar;
    private NavigationView navigationView;
    private View headerView;
    private HostListManager manager = HostListManager.getInstance();
    private int currentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_dashboard);
        initInstance();
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.contentContainer, DashboardFragment.newInstance())
                    .commit();
        }
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    private void initInstance() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
        setSupportActionBar(toolbar);
        navigationView = (NavigationView) findViewById(R.id.navigationDrawer);
        navigationView.setNavigationItemSelectedListener(this);

        headerView = navigationView.getHeaderView(0);
        TextView tvNameProfile = (TextView) headerView.findViewById(R.id.tvUsername);
        TextView tvLastLogin = (TextView) headerView.findViewById(R.id.tvLasLogin);
        tvNameProfile.setText(manager.getDao().getUsername());
        tvLastLogin.setText(manager.getDao().getDate());

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        actionBarDrawerToggle = new ActionBarDrawerToggle(DashboardActivity.this, drawerLayout
                , R.string.open_drawer, R.string.close_drawer);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (currentFragment != id) {
            switch (item.getItemId()) {
                case R.id.dashboard:
                    getSupportFragmentManager().beginTransaction()
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                            .replace(R.id.contentContainer, DashboardFragment.newInstance())
                            .commit();
                    break;
                case R.id.about:
                    getSupportFragmentManager().beginTransaction()
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                            .replace(R.id.contentContainer, AboutFragment.newInstance())
                            .commit();
                    break;
                case R.id.logout:
                    LayoutInflater inflater;
                    inflater = LayoutInflater.from(DashboardActivity.this);
                    final View logOutDialog = inflater.inflate(R.layout.dialog_logout, null);

                    AlertDialog dialog = new AlertDialog.Builder(DashboardActivity.this)
                            .setView(logOutDialog)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener(){
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    startActivity(new Intent(DashboardActivity.this, MainActivity.class));
                                    finish();
                                }
                            })
                            .setNegativeButton("Cancel", null).create();
                    dialog.show();
                    break;
            }
        }

        if (id != R.id.logout) {
            currentFragment = id;
        }
        invalidateOptionsMenu();
        drawerLayout.closeDrawers();
        return true;
    }
}
