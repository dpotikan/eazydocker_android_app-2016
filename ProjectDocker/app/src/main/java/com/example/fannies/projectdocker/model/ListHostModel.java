package com.example.fannies.projectdocker.model;

/**
 * Created by Fannies on 1/19/2017.
 */

public class ListHostModel {
    private String image;
    private String hostName;
    private String hostIp;

    public ListHostModel(){
        hostName = "Host Name";
        hostIp = "Host Ip";
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getHostIp() {
        return hostIp;
    }

    public void setHostIp(String hostIp) {
        this.hostIp = hostIp;
    }
}
