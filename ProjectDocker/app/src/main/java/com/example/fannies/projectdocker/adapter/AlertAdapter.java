package com.example.fannies.projectdocker.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.example.fannies.projectdocker.dao.DockerContainerPeakDao;
import com.example.fannies.projectdocker.dao.DockerContainerProcessCollectionDao;
import com.example.fannies.projectdocker.manager.DockerContainerPeakManager;
import com.example.fannies.projectdocker.view.AlertListItem;

/**
 * Created by Fannies on 1/26/2017.
 */

public class AlertAdapter extends BaseAdapter {
    @Override
    public int getCount() {
        if (DockerContainerPeakManager.getInstance().getDao() == null) {
            return 0;

        }
        if (DockerContainerPeakManager.getInstance().getDao().getData() == null) {
            return 0;
        }
        return DockerContainerPeakManager.getInstance().getDao().getData().size();
    }

    @Override
    public Object getItem(int position) {
        return DockerContainerPeakManager.getInstance().getDao().getData().get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        AlertListItem item;
        if (convertView != null) {
            item = (AlertListItem) convertView;
        } else {
            item = new AlertListItem(parent.getContext());
        }
        DockerContainerPeakDao dao = (DockerContainerPeakDao) getItem(position);

        if (dao != null) {
            item.setTvContainerName(dao.getcName());
            //item.setTvImage();
            if (dao.getCpuPer() != null && dao.getmPer() == null) {
                item.setTvCause("CPU Alert : " + dao.getCpuPer());
            } else if (dao.getmPer() != null && dao.getCpuPer() == null) {
                item.setTvCause("Memory Alert : " + dao.getmPer());
            } else if (dao.getCpuPer() != null && dao.getmPer() != null) {
                item.setTvCause("CPU Alert : " + dao.getCpuPer() + " and " + "Memory Alert : " + dao.getmPer());
            }
            item.setTvDate(dao.getDate());
        }
        return item;
    }
}
