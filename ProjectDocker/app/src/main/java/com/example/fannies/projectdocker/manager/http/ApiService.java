package com.example.fannies.projectdocker.manager.http;

import com.example.fannies.projectdocker.adapter.ContainerAdapter;
import com.example.fannies.projectdocker.dao.AddHostCheckDao;
import com.example.fannies.projectdocker.dao.DockerContainerCollectionDao;
import com.example.fannies.projectdocker.dao.DockerContainerEachStatCollectionDao;
import com.example.fannies.projectdocker.dao.DockerContainerPeakCollectionDao;
import com.example.fannies.projectdocker.dao.DockerContainerProcessCollectionDao;
import com.example.fannies.projectdocker.dao.DockerContainerStatisticCollectionDao;
import com.example.fannies.projectdocker.dao.DockerHostDao;
import com.example.fannies.projectdocker.dao.HostItemCollectionDao;
import com.example.fannies.projectdocker.dao.SetThesholdCheckDao;
import com.example.fannies.projectdocker.dao.SignUpCheckDao;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by Fannies on 1/19/2017.
 */

public interface ApiService {
    @FormUrlEncoded
    @POST("login.py?")
    Call<HostItemCollectionDao> loadUserLogin(
            @Field("username") String username,
            @Field("password") String password
    );
    @FormUrlEncoded
    @POST("addhost.py?")
    Call<AddHostCheckDao> addDockerHost(
            @Field("url") String url,
            @Field("port") String port,
            @Field("name") String name,
            @Field("ids") int ids
    );
    @FormUrlEncoded
    @POST("api_getcontainerall.py?")
    Call<DockerContainerCollectionDao> listDockerContainer(
            @Field("url") String url,
            @Field("port") String port
    );
    @FormUrlEncoded
    @POST("api_pause.py?")
    Call<Void> pauseDockerContainer(
            @Field("url") String url,
            @Field("port") String port,
            @Field("container") String container
    );
    @FormUrlEncoded
    @POST("api_unpause.py?")
    Call<Void> unPauseDockerContainer(
            @Field("url") String url,
            @Field("port") String port,
            @Field("container") String container
    );
    @FormUrlEncoded
    @POST("api_restart.py?")
    Call<Void> restartDockerContainer(
            @Field("url") String url,
            @Field("port") String port,
            @Field("container") String container
    );
    @FormUrlEncoded
    @POST("api_start.py?")
    Call<Void> startDockerContainer(
            @Field("url") String url,
            @Field("port") String port,
            @Field("container") String container
    );
    @FormUrlEncoded
    @POST("api_stop.py?")
    Call<Void> stopDockerContainer(
            @Field("url") String url,
            @Field("port") String port,
            @Field("container") String container
    );
    @FormUrlEncoded
    @POST("api_rename.py?")
    Call<Void> renameContainer(
            @Field("url") String url,
            @Field("port") String port,
            @Field("container") String container,
            @Field("name") String name
    );
    @FormUrlEncoded
    @POST("api_deleteContainer.py?")
    Call<Void> deleteDockerHost(
            @Field("url") String url,
            @Field("port") String port,
            @Field("ids") int ids
    );
    @FormUrlEncoded
    @POST("api_getProcess.py?")
    Call<DockerContainerProcessCollectionDao> getProcess(
            @Field("url") String url,
            @Field("port") String port,
            @Field("container") String container
    );
    @FormUrlEncoded
    @POST("api_graph_data.py?")
    Call<DockerContainerStatisticCollectionDao> getStatistic(
            @Field("url") String url,
            @Field("port") String port
    );
    @FormUrlEncoded
    @POST("api_peak.py?")
    Call<DockerContainerPeakCollectionDao> getPeak(
            @Field("url") String url,
            @Field("port") String port
    );
    @FormUrlEncoded
    @POST("api_teststat.py?")
    Call<DockerContainerEachStatCollectionDao> getEachStat(
            @Field("url") String url,
            @Field("port") String port
    );
    @FormUrlEncoded
    @POST("user_sign.py?")
    Call<SignUpCheckDao> signUp(
            @Field("username") String username,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("api_settheshold.py?")
    Call<SetThesholdCheckDao> setTheshold(
            @Field("url") String hostUrl,
            @Field("port") String hostPort,
            @Field("container") String cName,
            @Field("setcpu") float cpu,
            @Field("setmem") float mem
    );


}
