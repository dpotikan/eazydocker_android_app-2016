package com.example.fannies.projectdocker.dao;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fannies on 1/23/2017.
 */

public class DockerContainerProcessCollectionDao {
    @SerializedName("Host") private List<DockerContainerProcessDao> host = new ArrayList<>();

    public List<DockerContainerProcessDao> getHost() {
        return host;
    }

    public void setHost(List<DockerContainerProcessDao> host) {
        this.host = host;
    }
}
