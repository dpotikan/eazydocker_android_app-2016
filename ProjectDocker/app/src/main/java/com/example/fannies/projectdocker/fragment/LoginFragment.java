package com.example.fannies.projectdocker.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fannies.projectdocker.R;
import com.example.fannies.projectdocker.activity.DashboardActivity;
import com.example.fannies.projectdocker.activity.SignupActivity;
import com.example.fannies.projectdocker.dao.HostItemCollectionDao;
import com.example.fannies.projectdocker.manager.HttpManager;
import com.example.fannies.projectdocker.manager.HostListManager;
import com.example.fannies.projectdocker.manager.UserManager;
import com.inthecheesefactory.thecheeselibrary.manager.Contextor;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by nuuneoi on 11/16/2014.
 */
@SuppressWarnings("unused")
public class LoginFragment extends Fragment implements View.OnClickListener {

    private UserManager manager;
    private EditText edtUsername;
    private EditText edtPassword;
    private Button btnLogin;
    private TextView tvSignup;

    public LoginFragment() {
        super();
    }

    @SuppressWarnings("unused")
    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);

        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);
        initInstances(rootView, savedInstanceState);
        return rootView;
    }

    private void init(Bundle savedInstanceState) {

        // Init Fragment level's variable(s) here
        manager = UserManager.getInstance();
    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {
        // Init 'View' instance(s) with rootView.findViewById here

        edtUsername = (EditText) rootView.findViewById(R.id.edtUsername);
        edtPassword = (EditText) rootView.findViewById(R.id.edtPassword);
        btnLogin = (Button) rootView.findViewById(R.id.btnLogin);
        tvSignup = (TextView) rootView.findViewById(R.id.tvSignup);

        btnLogin.setOnClickListener(this);
        tvSignup.setOnClickListener(this);

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance State here
    }

    public String getUsernameFromEditext() {
        return edtUsername.getText().toString();
    }

    public String getPasswordFromEditext() {
        return edtPassword.getText().toString();
    }

    @Override
    public void onClick(View v) {
        if (v == btnLogin) {
            final ProgressDialog progressDialog;
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setTitle(getContext().getResources().getString(R.string.loading));
            progressDialog.show();

            Call<HostItemCollectionDao> call = HttpManager.getInstance().getServeice().loadUserLogin(edtUsername.getText().toString()
                    , edtPassword.getText().toString());
            call.enqueue(new Callback<HostItemCollectionDao>() {
                @Override
                public void onResponse(Call<HostItemCollectionDao> call, Response<HostItemCollectionDao> response) {
                    if(response.isSuccessful()){
                        HostItemCollectionDao dao = response.body();
                        if(dao.getStatus()){
                            HostListManager.getInstance().setDao(dao);
                            progressDialog.dismiss();
                            Toast.makeText(Contextor.getInstance().getContext(),"Login success",Toast.LENGTH_SHORT).show();

                            startActivity(new Intent(Contextor.getInstance().getContext(),DashboardActivity.class));
                            getActivity().finish();
                        }
                        else{
                            progressDialog.dismiss();
                            Toast.makeText(Contextor.getInstance().getContext(),"Wrong username or password",Toast.LENGTH_SHORT).show();
                        }
                    }
                    else{
                        progressDialog.dismiss();
                        Toast.makeText(Contextor.getInstance().getContext(),"HTTP NOT FOUND",Toast.LENGTH_SHORT).show();
                    }
                }
                @Override
                public void onFailure(Call<HostItemCollectionDao> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(Contextor.getInstance().getContext()
                            ,getContext().getResources().getString(R.string.onFailure)
                            ,Toast.LENGTH_SHORT)
                            .show();
                }
            });
        }
        if (v == tvSignup) {
            startActivity(new Intent(getActivity().getBaseContext(), SignupActivity.class));
        }
    }
}
