package com.example.fannies.projectdocker.manager;

import android.content.Context;

import com.example.fannies.projectdocker.dao.DockerContainerProcessCollectionDao;
import com.inthecheesefactory.thecheeselibrary.manager.Contextor;

/**
 * Created by nuuneoi on 11/16/2014.
 */
public class DockerContainerProcessManager {

    private static DockerContainerProcessManager instance;
    private DockerContainerProcessCollectionDao dao;

    public static DockerContainerProcessManager getInstance() {
        if (instance == null)
            instance = new DockerContainerProcessManager();
        return instance;
    }

    private Context mContext;

    private DockerContainerProcessManager() {
        mContext = Contextor.getInstance().getContext();
    }

    public DockerContainerProcessCollectionDao getDao() {
        return dao;
    }

    public void setDao(DockerContainerProcessCollectionDao dao) {
        this.dao = dao;
    }
}
