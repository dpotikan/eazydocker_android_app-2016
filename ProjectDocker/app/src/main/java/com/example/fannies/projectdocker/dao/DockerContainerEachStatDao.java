package com.example.fannies.projectdocker.dao;

import com.google.gson.annotations.SerializedName;

/**
 * Created by NARIN on 27/1/2560.
 */

public class DockerContainerEachStatDao {
    @SerializedName("Cpu") private float cpu;
    @SerializedName("Mem") private float mem;
    @SerializedName("Name") private String name;
    @SerializedName("Net_o") private float net_o;

    public float getCpu() {
        return cpu;
    }

    public void setCpu(float cpu) {
        this.cpu = cpu;
    }

    public float getMem() {
        return mem;
    }

    public void setMem(float mem) {
        this.mem = mem;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getNet_o() {
        return net_o;
    }

    public void setNet_o(float net_o) {
        this.net_o = net_o;
    }
}
