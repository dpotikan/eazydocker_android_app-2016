package com.example.fannies.projectdocker.manager;

import android.content.Context;

import com.example.fannies.projectdocker.dao.DockerContainerPeakCollectionDao;
import com.inthecheesefactory.thecheeselibrary.manager.Contextor;

/**
 * Created by nuuneoi on 11/16/2014.
 */
public class DockerContainerPeakManager {

    private static DockerContainerPeakManager instance;
    private DockerContainerPeakCollectionDao dao;
    public static DockerContainerPeakManager getInstance() {
        if (instance == null)
            instance = new DockerContainerPeakManager();
        return instance;
    }

    private Context mContext;

    private DockerContainerPeakManager() {
        mContext = Contextor.getInstance().getContext();
    }

    public DockerContainerPeakCollectionDao getDao() {
        return dao;
    }

    public void setDao(DockerContainerPeakCollectionDao dao) {
        this.dao = dao;
    }
}
