package com.example.fannies.projectdocker.manager;

import android.content.Context;

import com.example.fannies.projectdocker.dao.DockerContainerStatisticCollectionDao;
import com.inthecheesefactory.thecheeselibrary.manager.Contextor;

/**
 * Created by nuuneoi on 11/16/2014.
 */
public class DockerContainerStatisticManager {

    private static DockerContainerStatisticManager instance;
    private DockerContainerStatisticCollectionDao dao;

    public static DockerContainerStatisticManager getInstance() {
        if (instance == null)
            instance = new DockerContainerStatisticManager();
        return instance;
    }

    private Context mContext;

    private DockerContainerStatisticManager() {
        mContext = Contextor.getInstance().getContext();
    }

    public DockerContainerStatisticCollectionDao getDao() {
        return dao;
    }

    public void setDao(DockerContainerStatisticCollectionDao dao) {
        this.dao = dao;
    }
}
