package com.example.fannies.projectdocker.dao;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Fannies on 1/24/2017.
 */

public class AddHostCheckDao {
    @SerializedName("Status")   private Boolean status;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
