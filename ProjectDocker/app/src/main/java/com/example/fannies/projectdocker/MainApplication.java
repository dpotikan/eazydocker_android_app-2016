package com.example.fannies.projectdocker;

import android.app.Application;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.LinearLayoutCompat;

import com.inthecheesefactory.thecheeselibrary.manager.Contextor;

/**
 * Created by Fannies on 1/15/2017.
 */

public class MainApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Contextor.getInstance().init(getApplicationContext());
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
