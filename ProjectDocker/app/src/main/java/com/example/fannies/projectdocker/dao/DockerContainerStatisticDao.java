package com.example.fannies.projectdocker.dao;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Fannies on 1/24/2017.
 */

public class DockerContainerStatisticDao {
    @SerializedName("Date")     private String date;
    @SerializedName("M_per")    private float mPer;
    @SerializedName("Cpu_per")  private float cpuPer;
    @SerializedName("Net_o")    private float netOut;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public float getmPer() {
        return mPer;
    }

    public void setmPer(int mPer) {
        this.mPer = mPer;
    }

    public float getCpuPer() {
        return cpuPer;
    }

    public void setCpuPer(int cpuPer) {
        this.cpuPer = cpuPer;
    }

    public float getNetOut() {
        return netOut;
    }

    public void setNetOut(int netOut) {
        this.netOut = netOut;
    }
}
