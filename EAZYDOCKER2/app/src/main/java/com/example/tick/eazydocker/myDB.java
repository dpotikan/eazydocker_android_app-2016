package com.example.tick.eazydocker;

/**
 * Created by tick on 10/16/2016.
 */
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.util.Log;


public class myDB extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    public static final String DATABASE_NAME = "mydatabase1";

    // Table Name
    public static final String TABLE_MEMBER = "members";
    public static final String NAME = "NAME";
    public static final String IP = "IP";
    public static final String PORT = "PORT";
    public static final String ID = "MemberID";


    public myDB(Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        // TODO Auto-generated constructor stub
    }


    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_MEMBER +
                "(" + ID +  "INTEGER PRIMARY KEY AUTOINCREMENT," +
                NAME + " TEXT(100)," +
                IP + " INTEGER," +
                PORT + " INTEGER);");

        Log.d("CREATE TABLE", "Create Table Successfully.");

    }

    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
    }

    //Insert Data
    public long InsertData(String strMemberID, String strName, String strTel) {
        // TODO Auto-generated method stub

        try {
            SQLiteDatabase db;
            db = this.getWritableDatabase(); // Write Data

            /**
             *  for API 11 and above
             SQLiteStatement insertCmd;
             String strSQL = "INSERT INTO " + TABLE_MEMBER
             + "(MemberID,Name,Tel) VALUES (?,?,?)";

             insertCmd = db.compileStatement(strSQL);
             insertCmd.bindString(1, strMemberID);
             insertCmd.bindString(2, strName);
             insertCmd.bindString(3, strTel);
             return insertCmd.executeInsert();
             */

            ContentValues Val = new ContentValues();
            Val.put("NAME", strMemberID);
            Val.put("IP", strName);
            Val.put("PORT", strTel);

            long rows = db.insert(TABLE_MEMBER, null, Val);

            db.close();
            return rows; // return rows inserted.

        } catch (Exception e) {
            return -1;
        }

    }

    public int DeleteData(int position) {
        // TODO Auto-generated method stub

        try {
            SQLiteDatabase db;
            db = this.getWritableDatabase(); // Write Data
            String position1 = String.valueOf(position);
            db.execSQL("delete from " + TABLE_MEMBER + " where MemberID =" + position);

            db.close();
            return 2;

        } catch (Exception e) {
            return -1;
        }


    }
}
