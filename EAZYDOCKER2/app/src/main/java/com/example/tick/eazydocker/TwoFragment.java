package com.example.tick.eazydocker;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tick.eazydocker.R;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.model.CategorySeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;
import org.achartengine.renderer.XYMultipleSeriesRenderer;


public class TwoFragment extends Fragment {


    TextView textView;
    OneFragment one;
    SQLiteDatabase data;
    private View mView;

    Button bt;

    private void initData() {
        String[] codename = {
                "KitKat", "Jelly Bean", "Ice Cream Sandwich",
                "Gingerbread", "Froyo"
        };

        double[] values = {13.6, 58.4, 12.3, 14.9, 0.8 };
        String[] colors = {
                "#ff4444", "#99cc00", "#aa66cc", "#33b5e5", "#ffbb33"
        };

        CategorySeries series = new CategorySeries("Android Platform Version");
        int length = codename.length;
        for (int i = 0; i < length; i++) {
            series.add(codename[i], values[i]);
        }

        DefaultRenderer renderer = new DefaultRenderer();
        for (int i = 0; i < length; i++) {
            SimpleSeriesRenderer seriesRenderer = new SimpleSeriesRenderer();
            seriesRenderer.setColor(Color.parseColor(colors[i]));

            renderer.addSeriesRenderer(seriesRenderer);
        }

        renderer.setChartTitleTextSize(60);
        renderer.setChartTitle("Android Platform Version");
        renderer.setLabelsTextSize(30);
        renderer.setLabelsColor(Color.GRAY);
        renderer.setLegendTextSize(30);
        renderer.setExternalZoomEnabled(false);

        drawChart(series, renderer);

    }
    private void drawChart(CategorySeries series,
                           DefaultRenderer renderer) {
        GraphicalView graphView =
                ChartFactory.getPieChartView(getActivity(), series, renderer);

        RelativeLayout container =
                (RelativeLayout) mView.findViewById(R.id.graph_container);

        container.addView(graphView);
    }




    public TwoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root =  inflater.inflate(R.layout.fragment_two, container, false);
        mView = root;
        initData();

        bt = (Button)root.findViewById(R.id.button2);
        bt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                data();



            }
        });


        //textView = (TextView) getView().findViewById(R.id.textView4);
        //textView.setText(data);
        //String aa = one.Selected;


        return root;




    }


    public void data(){
        myDB2 aa = new myDB2(getActivity());
        data = aa.getWritableDatabase();
        Cursor mCursor = data.rawQuery("Select * from " + myDB2.TABLE_MEMBER, null);
        mCursor.moveToLast();
        String data22 = mCursor.getString(mCursor.getColumnIndex("SELECTED"));
        textView = (TextView) getView().findViewById(R.id.textView4);
        textView.setText(data22);
        data.execSQL("delete from "+ myDB2.TABLE_MEMBER);
    }

    public void delete(){

    }



}
